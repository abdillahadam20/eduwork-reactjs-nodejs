const express = require('express')
const fs = require('fs')
const path = require('path')
const app = express()
const port = 3000
// const router = require('./router')
// const log = require('./middleware/logger')

// app.use(log())
// app.use(router)

app.listen(port, () => {
    console.log('Server port' + port)
})


app.use(express.static('public'));

router.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'views', 'index.html'));
});

router.get('/about', (req, res) => {
    res.sendFile(path.join(__dirname, 'views', 'about.html'));
});

// Halaman products
app.get('/products', (req, res) => {
    fs.readFile(path.join(__dirname, 'data', 'products.json'), 'utf8', (err, data) => {
      if (err) {
        return res.status(500).send('Error reading data');
      }
      const products = JSON.parse(data);
      res.json(products);
    });
  });