const router = require('express').Router()

router.get('/', (req, res) => {
    const [page, total ] = req.query
    res.send({
        status: "Sukses",
        message: 'Welcome',
        page,
        total
    })
})

router.get('/products/:id', (req, res) => {
    res.json({
        id: req.params.id
    })
})

router.get('/category/:tag', (req, res) => {
    const {category, tag} = req.params

    res.json(category, tag)
})



module.exports = router;