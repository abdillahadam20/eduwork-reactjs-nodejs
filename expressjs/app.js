const express = require('express')
const app = express()
const port = 3000
// const productRouter = require('./app/product/routes')
// const productRouterV2 = require('./app/product_v2/routes')
// const productRouterV3 = require('./app/product_v3/routes')
// const productRouterV4 = require('./app/product_v4/routes')
const productRouteMongoDB = require('./app/mongodb/product/routes')
const logger = require('morgan')
const path = require('path')


app.use(logger('dev'))
// app.use('/api/v1',productRouter)
// app.use('/api/v2',productRouterV2)
// app.use('/api/v3',productRouterV3)
// app.use('/api/v4',productRouterV4)



app.use('/api/v1', productRouteMongoDB)


app.use(express.urlencoded({extended: true}))
app.use(express.json())
app.use('/public', express.static(path.join(__dirname, 'uploads')))
app.use(log)
app.use((err, req, res, next) => {
    res.status(404)
    res.send({
        status: 'Error',
        message: err.message
    })
})
app.listen(port, () => "Server running in http://localhost:3000")
