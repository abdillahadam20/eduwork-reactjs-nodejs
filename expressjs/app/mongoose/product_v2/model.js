const mongoose = require('mongoose')


const productSchema = mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Nama harus di isi']
    },
    price: {
        type: Number,
        required: true
    },
    stock: {
        type: Number,
        required: true
    },
    status: {
        type: Boolean,
        required: true
    },
    image_url: {
        type: String
    }
})


const Product = mongoose.model('Product', productSchema)
module.exports = Product