const { ObjectId } = require('mongodb')
const db = require('../../config/mongodb')

const index = (req, res) => {
    db.collection('products').find()
    .toArray()
    .then(result => res.send(result))
    .catch(error => res.send(error))
}

const view = (req, res) => {
    const {id} = req.params
    db.collection('products').findOne({_id: ObjectId(id)})
    .then(result => res.send(result))
    .catch(error => res.send(error))
}

module.exports = {index, view}
