const sequelize = require('../../config/sequelize')
const { Sequelize, DataTypes} = require('sequelize')


const Product = sequelize.define('Product', {
    user_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    location: {
        type: DataTypes.STRING,
        allowNull: false
    },
     status: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false
     },
     image_url: {
        type: DataTypes.TEXT
     }
})

module.exports = Product