const router = require('express').Router()
const Product = require('./model')
const multer = require('multer')
const upload = multer({dest: 'uploads'})
const path = require('path')
const fs = require('fs')

router.post('/category', async (req, res) => {
    const {user_id, name, status, location} = req.body;
    const image = req.file
    if(image) {
        const target = path.join(__dirname, '../../uploads', image.originalname)
        fs.renameSync(image.path, target)
    }
    try {
        await Product.sync()
        const result = await Product.create({
            user_id,
            name,
            location,
            status,
            image_url: `http://localhost:3000/public/${image.originalname}`
        })
        res.send(result)
    } catch(e) {
        res.send(e)
    }
})

module.exports = router