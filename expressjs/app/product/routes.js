const router = require('express').Router()
const multer = require('multer')
const uploads = multer({ dest: 'uploads'})
const path = require('path')
const fs = require('fs')
const connection = require('../../config/mysql')
const productController = require('./controller')

router.get('/product', productController.index)
router.get('/product/:id', productController.view)
router.post('/product', upload.single('image'), productController.store)
router.put('/product/:id', upload.single('image') ,productController.update)
router.delete('/product/:id', upload.single('image'), productController.destroy)

module.exports = router