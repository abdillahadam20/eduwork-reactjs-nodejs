const connection = require('../../config/mysql')
const path = require('path')
const fs = require('fs')

const index = (req, res) => {
    const {search} = req.query;
    let exec = {}
    if(search) {
        exec = {
            sql: 'SELECT * FROM products WHERE name LIKE ?',
            values: [`%${search}%`]
        }
    } else {
        exec = {
            sql: `SELECT * FROM products`,
        }
    }
    connection.query(exec, _response(res))
}

const view = (req, res) => {
    connection.query({
        sql: 'SELECT * FROM products WHERE id = ?',
        values: [req.params.id]
    }, _response(res))
}

const destory = (req, res) => {
    connection.query({
        sql: 'DELETE FROM products WHERE id = ?',
        values: [req.params.id]
    }, _response(res))
}

const store = (req, res) => {
    const {user_id, name, price, stock, status} = req.body
    const image = req.file
    if(image) {
        const target = path.join(__dirname, '../../uploads', image.originalName)
        fs.renameSync(image.path, target)
        connection.query({
            sql: 'INSERT INTO products (user_id, name, price, stock, status) VALUES (?, ?, ?, ?, ?',
            values: [parseInt(user_id), name, price, stock, status, `http://localhost:3000/public/${image.originalName}`]
        }, _response(res))
    }
}

const update = (req, res) => {
    const {user_id, name, price, stock, status} = req.body
    const image = req.file
    let sql = ''
    let values = []
    if(image) {
        const target = path.join(__dirname, '../../uploads', image.originalName)
        fs.renameSync(image.path, target)
        sql = 'UPDATE products SET user_id = ?, name = ?, price = ?, stock = ?, status = ?, image_url = ? WHERE id = ?'
        values = [parseInt(user_id), name, price, stock, status, `http://localhost:3000/public/${image.originalName}`]
    } else {
        sql = 'UPDATE products SET user_id = ?, name = ?, price = ?, stock = ?, status = ? WHERE id = ?'
        values = [parseInt(user_id), name, price, stock, status]
    }
    connection.query({ sql, values }, _response(res))
}


const _response = (res) => {
    return (error, result) => {
        if(error) {
            res.send({
                status: 'Error',
                message: error.message
            })
        } else {
            res.send({
                status: 'Success',
                data: result
            })
        }
    }
}


module.exports = { index, view, store, update, destory }