const router = require('express').Router()
const Product = require('./model')
const multer = require('multer')
const upload = multer({dest: 'uploads'})
const path = require('path')
const fs = require('fs')

router.post('/product', async (req, res) => {
    const {user_id, price, stock, status} = req.body;
    const image = req.file
    if(image) {
        const target = path.join(__dirname, '../../uploads', image.originalname)
        fs.renameSync(image.path, target)
    }
    try {
        await Product.sync()
        const result = await Product.create({
            user_id,
            name,
            price,
            stock,
            status,
            image_url: `http://localhost:3000/public/${image.originalname}`
        })
    } catch(e) {
        res.send(er)
    }
})

module.exports = router