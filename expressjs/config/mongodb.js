const {MongoClient} = require('mongodb')
const url = 'mongodb://test:test@localhost:27017?authSource=admin'
const client = new MongoClient(url, {
    useUnifiedTopology: true
})

(async () => {
    try {
        await client.connect()
        console.log('Koneksi berhasil')
    } catch(e) {
        console.error(e)
    }
})()

const db = client.db('express')

module.exports = db