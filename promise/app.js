const apiKey = 'c22c9e3ffcd54fc1b791f1d965917f82';
const urlBase = `https://newsapi.org/v2/top-headlines?country=id&category=sports&apiKey=${apiKey}`;
const root = document.getElementById('root');
const search = document.getElementById('search');

// Fething data
async function fetchData() {
  try {
    const data = await fetch(urlBase);
    const jsonData = await data.json();
    return jsonData;
  } catch (error) {
    console.error(error);
    root.innerHTML = '<p>Terjadi kesalahan saat mengambil data berita.</p>'; 
  }
}

// Render data kedalam html
async function renderData() {
  const data = await fetchData();
  if (data && data.articles) {
    const news = data.articles;
    const newsList = news.map((item) => {
      return `
        <div class="card container" style="width: 15rem;>
					<img src="${item.urlToImage}" class="card-img-top" alt="..." />
          <div class="card-body">
            <h5 class="card-title">${item.title}</h5>
            <p class="card-text">${item.description}</p>
            <a href="${item.url}" class="btn btn-primary">Read More</a>
          </div>
        </div>
      `;
    });
    root.innerHTML = newsList.join('');
  } 
}

// Untuk memfilter pencarian
function filter() {
  const filterText = search.value.toLowerCase(); // buat semua huruf menjadi huruf kecil
  const cards = document.querySelectorAll('.card'); // Ambil semua class card yang ada dalam component
  
	// Looping menggunakan for each
  cards.forEach((card) => {
    const title = card.querySelector('.card-title').textContent.toLowerCase();
    const description = card.querySelector('.card-text').textContent.toLowerCase();
    
    if (title.includes(filterText) || description.includes(filterText)) {
      card.style.display = '';
    } else {
      card.style.display = 'none';
    }
  });
}

// Event untuk search input
search.addEventListener('input', filter);

// Render data
renderData();