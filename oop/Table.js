class Table {

	//  Inisialisasi constructor buat class
  constructor(init) {
    this.init = init;
  }

  // Buat class untuk menyiapkan head table
  createHeader(data) {
    let open = "<thead><tr>";
    let close = "</tr></thead>";
    data.forEach((d) => {
      open += `<th>${d}</th>`;
    });

    return open + close;
  }

  // Buat class untuk menyiapkan body table
  createBody(data) {
    let open = "<tbody>";
    let close = "</tbody>";

    data.forEach((d) => {
      open += `
        <tr>
          <td>${d[0]}</td>
          <td>${d[1]}</td>
          <td>${d[2]}</td>
        </tr>
      `;
    });

    return open + close;
  }

  // Render elemen seperti menggunakan reactjs
  render(element) {
    let table =
      "<table class='table table-hover'>" +
      this.createHeader(this.init.columns) +
      this.createBody(this.init.data) +
      "</table>";
    element.innerHTML = table;
  }
}

// siapkan objek untuk mengisi tablenya
const table = new Table({
	columns: ["Name", "Email", "Phone Number"],
	data: [
    ['John', 'john@example.com', '(353) 01 222 3333'],
    ['Mark', 'mark@gmail.com',   '(01) 22 888 4444'],
    ['Eoin', 'eo3n@yahoo.com',   '(05) 10 878 5554'],
    ['Nisen', 'nis900@gmail.com',   '313 333 1923']
  ]
});

const app = document.querySelector("#app");
table.render(app);