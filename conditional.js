const products = [
    { name: "Laptop", price: 1000, category: "Electronics", available: true },
    { name: "Smartphone", price: 700, category: "Electronics", available: false },
    { name: "Tablet", price: 500, category: "Electronics", available: true },
    { name: "Headphones", price: 150, category: "Accessories", available: true }
  ];
  
  // 1. Conditional Variable
  // Menggunakan ternary operator untuk menentukan status ketersediaan produk
  const availabilityStatus = products.map(product => 
    product.available ? `${product.name} is available` : `${product.name} is out of stock`
  );
  
  console.log("Conditional Variable (Availability Status):");
  availabilityStatus.forEach(status => console.log(status));
  console.log('---');
  
  // 2. Map
  // Menggunakan map untuk membuat array baru yang hanya berisi nama produk
  const productNames = products.map(product => product.name);
  
  console.log("Map (Product Names):");
  console.log(productNames); // ["Laptop", "Smartphone", "Tablet", "Headphones"]
  console.log('---');
  
  // 3. Filter
  // Menggunakan filter untuk mendapatkan produk yang tersedia (available)
  const availableProducts = products.filter(product => product.available);
  
  console.log("Filter (Available Products):");
  availableProducts.forEach(product => {
    console.log(`${product.name} - $${product.price}`);
  });
  console.log('---');
  
  // Menggunakan filter untuk mendapatkan produk dengan harga lebih dari 500
  const expensiveProducts = products.filter(product => product.price > 500);
  
  console.log("Filter (Expensive Products > $500):");
  expensiveProducts.forEach(product => {
    console.log(`${product.name} - $${product.price}`);
  });
  console.log('---');
  