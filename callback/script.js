

const tbody = document.querySelector('.data') // Ambil element html class data
const loadingIndicator = document.createElement('div'); // Buat elemen untuk indikator loading
loadingIndicator.textContent = 'Loading...'; // Buat nama isi dari div loadingIdicator
loadingIndicator.style.textAlign = 'center'; // Styling class text align center

// Buat function untuk fetching datanya
function fetchingData(callback) {
  tbody.appendChild(loadingIndicator); // Munculkan menggunakan appendChild

  fetch(`https://jsonplaceholder.typicode.com/users`)
  .then(res => res.json())
  .then(data => {
    // Buat loading indicator menggunakan setTimeOut dari Asyncronous
    setTimeout(() => {
      tbody.removeChild(loadingIndicator); // Remove ketika datanya sudah tampil
      callback(data) // Buat Callback functin yang mempunyai parameter data
    }, 5000) // Set selama 5 detik
  }).catch(error => {
    loadingIndicator.textContent = 'Error fetching data. Please try again later.'; // Jika fetching error muncul pesan berikut
    console.error(error)
  })
}

// Function callback untuk merender data
function renderData(data) {
  // looping semua datanya menggunakan forEach
  data.forEach(item => {
    const tr = document.createElement('tr');

    // Buat elemen td untuk setiap kolom data yang ada di API
    const idTd = document.createElement('td');
    idTd.textContent = item.id;
    tr.appendChild(idTd);

    const nameTd = document.createElement('td');
    nameTd.textContent = item.name;
    tr.appendChild(nameTd);

    const usernameTd = document.createElement('td');
    usernameTd.textContent = item.username;
    tr.appendChild(usernameTd);

    const emailTd = document.createElement('td');
    emailTd.textContent = item.email;
    tr.appendChild(emailTd)

    const addressTd = document.createElement('td');
    addressTd.textContent = `${item.address.street} ${item.address.suite} ${item.address.city}`;
    tr.appendChild(addressTd)

    const companyTd = document.createElement('td');
    companyTd.textContent = item.company.name;
    tr.appendChild(companyTd)

    // tampilkan semunya ke dalam elemen tbody
    tbody.appendChild(tr);
  })
}

// Panggil function fetchingdata dan masukkan function renderdata kedalamnya
fetchingData(renderData)
