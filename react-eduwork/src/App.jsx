import { Link } from 'react-router-dom';
import './App.css';
import NewsListFunctional from './components/NewsListFunctional';
import About from "./pages/About";
import Header from './components/Header';
import Routing from './routing';
import Redux from './components/Redux'

function App() {
  return (
    <div className="App">
      {/* <Hero />
      <Features /> */}
      {/* <Form /> */}
      {/* <Header /> */}
      {/* <Routing /> */}
      <Redux />
      
      {/* <NewsList /> */}
      {/* <NewsListFunctional /> */}
    </div>
  );
}

export default App;
