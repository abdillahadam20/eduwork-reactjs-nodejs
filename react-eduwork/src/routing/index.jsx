import React from 'react';
import Homepage from '../pages/Homepage';
import About from '../pages/About';
import { Routes, Route } from 'react-router-dom';

const Routing = () => {
  return (
    <Routes>
      <Route path='/' element={<Homepage />} />
      <Route path='about' element={<About />} />
    </Routes>
  );
};

export default Routing;
