import React from 'react'
import { Link } from 'react-router-dom'

export default function Header() {
  return (
    <div>
      <header className="bg-blue-500 text-white p-4 text-center">
        <h1 className="text-3xl font-bold">News Portal</h1>
        <nav>
          <ul>
            <li>
              <Link to='/'>Homepage</Link>
            </li>
            <li>
              <Link to="/about">About</Link>
            </li>
          </ul>
        </nav>
      </header>
    </div>
  )
}
