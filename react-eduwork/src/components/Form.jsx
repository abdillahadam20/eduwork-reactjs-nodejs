import React from "react";
import * as Yup from 'yup';

export default function Form() {
//  Buat validasi schema menggunakan Yup
  const validationSchema = Yup.object().shape({
    username: Yup.string().required("Username is required").min(3, "Username must be at least 3 characters long").max(25, "Username must be at most 25 characters long"),
    email: Yup.string().email("Invalid email address").required("Email is required"),
    password: Yup.string().required("Password is required").min(8, "Password must be at least 8 characters long").max(25, "Password must be at most 25 characters long"),
  });

  // function handlesubmit
  const handleSubmit = async (e) => {
    e.preventDefault();

    // mengambil data dari value yang dimasukkan ke elemen input
    const form = {
      username: e.target.username.value,
      email: e.target.email.value,
      password: e.target.password.value,
    };

    try {
      // validasi from
      await validationSchema.validate(form, { abortEarly: false });

      // Jika validasi sukses
      console.log("Form data:", form); // tampilkan data ke console.log
      alert("Form submitted successfully!"); // menampilkan pesan sukses
    } catch (err) {
      // jika validasi error
      if (err.inner) {
        const errors = err.inner.map(error => error.message);
        alert(errors.join("\n"));
      }
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <input type="text" name="username" placeholder="Username" />
      <input type="email" name="email" placeholder="Email" />
      <input type="password" name="password" placeholder="Password" />
      <button type="submit">Create Account</button>
    </form>
  );
}
