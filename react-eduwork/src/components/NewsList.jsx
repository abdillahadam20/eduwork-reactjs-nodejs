import React, { Component } from 'react';
import NewsItem from './NewsItem';
import axios from 'axios';
import SearchBar from './SearchBar';

class NewsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      articles: [],
      query: '',
    };
  }

  componentDidMount() {
    this.fetchNews();
  }

  // Fetch data menggunakan axios
  fetchNews = () => {
    const { query } = this.state;
    const apiKey = 'c22c9e3ffcd54fc1b791f1d965917f82'; // API Key
    const url = `https://newsapi.org/v2/top-headlines?country=us&category=business${query ? `&q=${query}` : ''}&apiKey=${apiKey}`; // Search berita berdasarkan query jika bernilai false maka data querynya kosong

    axios.get(url)
      .then(response => {
        if (response.data.articles) {
          this.setState({ articles: response.data.articles }); // Ambil data dari artikel
        } else {
          this.setState({ articles: [] }); // Jika tidak ada tampilkan data kosong dengan nilai array
        }
      })
      .catch(error => {
        console.error('Error fetching the news:', error); // tampilkan error di console
      });
  }

  // Yang menghendel query search
  handleSearch = (query) => {
    this.setState({ query }, this.fetchNews);
  }

  render() {
    // render datanya di dalam dengan dibungkus variable
    const { articles } = this.state;

    return (
      <div className="container mx-auto p-4">
        <SearchBar onSearch={this.handleSearch} />
        <div className="grid grid-cols-1 md:grid-cols-3 gap-4">
          {articles.length > 0 ? (
            articles.map((article, index) => (
              <NewsItem article={article} key={index} />
            ))
          ) : (
            <p>No articles found. Please try again later.</p>
          )}
        </div>
      </div>
    );
  }
}

export default NewsList;
