import React, { useState, useEffect } from 'react';
import NewsItem from './NewsItem';
import axios from 'axios';
import SearchBar from './SearchBar';

const NewsListFunctional = () => {
  const [articles, setArticles] = useState([]);
  const [query, setQuery] = useState('');

  // Function to fetch news articles
  const fetchNews = async () => {
    const apiKey = 'c22c9e3ffcd54fc1b791f1d965917f82'; // API Key
    const url = `https://newsapi.org/v2/top-headlines?country=us&category=business${query ? `&q=${query}` : ''}&apiKey=${apiKey}`; // Search berita berdasarkan query jika bernilai false maka data querynya kosong

    try {
      const response = await axios.get(url);
      if (response.data.articles) {
        setArticles(response.data.articles); // Ambil data dari artikel
      } else {
        setArticles([]); // Jika tidak ada tampilkan data kosong dengan nilai array
      }
    } catch (error) {
      console.error('Error fetching the news:', error); // tampilkan error di console
    }
  };

  // fetch news ketika komponen di mount atau query berubah
  useEffect(() => {
    fetchNews();
  }, [query]); // Jalankan hanya jika query berubah

  // Handler for search input
  const handleSearch = (query) => {
    setQuery(query);
  };

  return (
    <div className="container mx-auto p-4">
      <SearchBar onSearch={handleSearch} />
      <div className="grid grid-cols-1 md:grid-cols-3 gap-4">
        {articles.length > 0 ? (
          articles.map((article, index) => (
            <NewsItem article={article} key={index} />
          ))
        ) : (
          <p>No articles found. Please try again later.</p>
        )}
      </div>
    </div>
  );
};

export default NewsListFunctional;
