import React, { Component } from 'react';

class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      query: '',
    };
  }

  // Menangani perubahan untuk live search
  handleChange = (event) => {
    const query = event.target.value;
    this.setState({ query });
    this.props.onSearch(query); 
  }

  render() {
    return (
      <div>
        <input
          type="text"
          placeholder="Search for news..."
          value={this.state.query} // Mengambil value dari input
          onChange={this.handleChange}  // handle change function
        />
      </div>
    );
  }
}

export default SearchBar;
