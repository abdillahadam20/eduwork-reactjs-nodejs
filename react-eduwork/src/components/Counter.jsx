import React from 'react'
import { useSelector} from 'react-redux'
import { useDispatch} from 'react-redux'
import { decrement, increment, decrementWithCheckingAction} from '../app/features/counter/actions'


const Redux = () => {
    let {count} = useSelector(state => state.counter)
    const dispatch = useDispatch()

    return (
        <div>
            <button onClick={() => dispatch(decrementWithCheckingAction(1))}>-</button>
            {' '}<span>{count.count}</span>{' '}
            <button onClick={() => dispatch(increment(1))}>+</button>
        </div>
    )
}

export default Redux;