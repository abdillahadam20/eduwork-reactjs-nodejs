const products = ["Laptop", "Smartphone", "Tablet", "Headphones"];

// 1. Destructuring Array
const [product1, product2, product3] = products;
console.log("Destructuring Array:");
console.log(product1);
console.log(product2);
console.log(product3);
console.log('---');

// Data produk dalam bentuk objek
const product = {
  name: "Laptop",
  price: 1000,
  category: "Electronics",
  available: true
};

// 2. Destructuring Object
const { name, price, category } = product;
console.log("Destructuring Object:");
console.log(name);
console.log(price);
console.log(category);
console.log('---');

// 3. Destructuring dengan Alias (Renaming)
const { name: productName, price: productPrice } = product;
console.log("Destructuring with Alias:");
console.log(productName);
console.log(productPrice);
console.log('---');

// 4. Destructuring dengan Default Value
const { stock = 50 } = product;
console.log("Destructuring with Default Value:");
console.log(stock);
console.log('---');

// 5. Destructuring dengan function
function displayProduct({ name, price, category }) {
  console.log("Destructuring in Function (Object):");
  console.log(`Product: ${name}`);
  console.log(`Price: $${price}`);
  console.log(`Category: ${category}`);
}
displayProduct(product);
console.log('---');

function displayProducts([firstProduct, secondProduct, thirdProduct]) {
  console.log("Destructuring in Function (Array):");
  console.log(firstProduct); 
  console.log(secondProduct);
  console.log(thirdProduct);
}
displayProducts(products);

